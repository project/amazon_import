<?php

function amazon_import_settings_form($form_state) {
  drupal_set_title(t('Amazon Import Settings'));
  $types = content_types();
  $type_options = array('' => t('Please select'));
  $field_options = array('' => t('Please select'));

  foreach ($types as $type_name => $type) {
    foreach ($type['fields'] as $field_name => $field ) {
      $has_asin = FALSE;
      if ($field['type'] == 'asin') {
        $field_options[$field_name] = $field_name;
        $has_asin = TRUE;
      }
      if ($has_asin) {
        $type_options[$type_name] = $type['name'];
      }
    }
  }
  
  $form['amazon_import_type'] = array(
    '#type' => 'select',
    '#title' => t('Amazon Content Type'),
    '#default_value' => variable_get('amazon_import_type', ''),
    '#options' => $type_options,
    '#description' => t('Choose the content type to use to create amazon products.'),
    '#required' => TRUE,
  );

  $form['amazon_import_field'] = array(
    '#type' => 'select',
    '#title' => t('Amazon ASIN field'),
    '#default_value' => variable_get('amazon_import_field', ''),
    '#options' => $field_options,
    '#description' => t('Choose the field used to store the Amazon ASIN code in.'),
    '#required' => TRUE,
  );

  $form['amazon_import_map_title'] = array(
    '#type' => 'checkbox',
    '#title' => t('Map Product Title to Node Title'),
    '#description' => t('When creating the node, should the title be set to that of the Amazon product?'),
    '#default_value' => variable_get('amazon_import_map_title', 0),
  );

  $form['amazon_import_map_body'] = array(
    '#type' => 'checkbox',
    '#title' => t('Map Editorial Review to Body'),
    '#description' => t('When creating the node, should the body be set to that of the Amazon editorial review?'),
    '#default_value' => variable_get('amazon_import_map_body', 0),
  );

  $period = drupal_map_assoc(array(0, 3600, 7200, 14400, 21600, 43200, 86400), 'format_interval');
  $form['amazon_import_cache_expire'] = array(
    '#type' => 'select',
    '#title' => t('Search result cache duration'),
    '#description' => t('Searches are cached for performance reasons, choose how long the cached requests last.'),
    '#default_value' => variable_get('amazon_import_cache_expire', 21600),
    '#options' => $period
  );


  return system_settings_form($form);
}

/**
 * Validation handler for amazon_settings_form().
 * Handles the file upload and making sure it happens correctly.
 */
function amazon_import_settings_form_validate($form, &$form_state) {

  $type_name = $form_state['values']['amazon_import_type'];
  $field_name = $form_state['values']['amazon_import_field'];

  if (empty($type_name) || empty($field_name)) {
    if (empty($type_name)) {
      form_set_error('amazon_import_type', t("Please choose a type to use for Amazon Imports."));
    }
    if (empty($field_name)) {
      form_set_error('amazon_import_field', t("Please choose an ASIN field to use for Amazon Imports."));
    }
    return;
  }

  $field = content_fields($field_name, $type_name);

  if (empty($field)) {
    form_set_error('amazon_import_field', t("Invalid or missing field."));
  }
  elseif ($field['type'] != 'asin')  {
    form_set_error('amazon_import_field', t("Import field must be of ASIN type."));
  }

}